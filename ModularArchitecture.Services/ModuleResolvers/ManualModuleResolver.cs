﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using ModularArchitecture.Core;

namespace ModularArchitecture.Services.ModuleResolvers
{
	/// <summary>
	/// Module resolver to manually register module types.
	/// </summary>
	/// <typeparam name="TModuleId">The type defining module IDs.</typeparam>
	public class ManualModuleResolver<TModuleId> : ModuleResolver<TModuleId>
	{
		private IList<Type> repository;

		protected override IList<Type> TypeRepository
		{
			get { return repository; }
		}

		public ManualModuleResolver()
		{
			repository = new List<Type>();
		}

		/// <summary>
		/// Registers a module type eligible for instantiation by the <c>ModuleService</c>.
		/// </summary>
		/// <typeparam name="TModule">The type defining a module.</typeparam>
		public void Register<TModule>() where TModule : class, IModule<TModuleId>
		{
			repository.Add(typeof(TModule));
		}
	}
}
