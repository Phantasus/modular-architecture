﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using ModularArchitecture.Core;

namespace ModularArchitecture.Services.ModuleResolvers
{
	/// <summary>
	/// Module resolver to register module types from DLLs.
	/// </summary>
	/// <typeparam name="TModuleId">The type defining module IDs.</typeparam>
	public class DllModuleResolver<TModuleId> : ModuleResolver<TModuleId>
	{
		private IList<Type> repository;

		protected override IList<Type> TypeRepository
		{
			get { return repository; }
		}

		public DllModuleResolver()
		{
			repository = new List<Type>();
		}

		/// <summary>
		/// Loads all available modules from all dynamic-link libraries found in the given path.
		/// <para />
		/// ATTENTION: Only visible modules (i.e. those defined as <c>public</c>) will be resolved!
		/// </summary>
		public void Initialize(string path)
		{
			if (Directory.Exists(path))
			{
				var dlls = Directory.GetFiles(path, "*.dll");
				var assemblies = dlls.Select(dll => Assembly.Load(AssemblyName.GetAssemblyName(dll)));
				var types = assemblies.SelectMany(assembly => assembly.GetExportedTypes());

				foreach (var type in types)
				{
					if (!type.IsInterface && !type.IsAbstract && typeof(IModule<TModuleId>).IsAssignableFrom(type))
					{
						repository.Add(type);
					}
				}
			}
			else
			{
				throw new DirectoryNotFoundException(string.Format("The provided path '{0}' does not exist!", path));
			}
		}
	}
}
