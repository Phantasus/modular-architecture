﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using ModularArchitecture.Core;

namespace ModularArchitecture.Services.Logging
{
	/// <summary>
	/// A simple base implementation of a logger, thread-safe.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining module IDs.</typeparam>
	public abstract class LogModule<TIdentifier> : IModule<TIdentifier>
	{
		private static readonly object @lock = new object();
		private IList<LogMessage> log;

		public abstract TIdentifier Id { get; }
		public abstract string Name { get; }

		public bool IsSingleton
		{
			get { return true; }
		}

		public LogModule()
		{
			log = new List<LogMessage>();
		}

		/// <summary>
		/// Logs a message with the given severity level.
		/// </summary>
		public void Log(LogLevel severity, string message)
		{
			lock (@lock)
			{
				var msg = new LogMessage
				{
					Date = DateTime.Now,
					Severity = severity,
					Message = message
				};

				log.Add(msg);
				LogUpdated(msg);
			}
		}

		/// <summary>
		/// Returns the current log history.
		/// </summary>
		public IList<LogMessage> RetrieveLog()
		{
			lock (@lock)
			{
				return new List<LogMessage>(log.Select(m => m.Clone()).Cast<LogMessage>());
			}
		}

		/// <summary>
		/// Clears the current log history.
		/// </summary>
		public void ClearLog()
		{
			lock (@lock)
			{
				log.Clear();
				LogCleared();
			}
		}

		protected abstract void LogUpdated(LogMessage message);
		protected abstract void LogCleared();
	}
}
