﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using ModularArchitecture.Core;

namespace ModularArchitecture.Services.Scaffolding
{
	/// <summary>
	/// An exemplary implementation of a module service as thread-safe singleton.
	/// </summary>
	class Service : ModuleService<Modules, Messages>
	{
		private static Service instance;
		private static readonly object @lock = new object();

		private Service()
		{
		}

		internal static Service Instance
		{
			get
			{
				lock (@lock)
				{
					return instance ?? (instance = new Service());
				}
			}
		}
	}
}
