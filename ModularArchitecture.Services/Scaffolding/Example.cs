﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using ModularArchitecture.Core.Communication;
using ModularArchitecture.Services.ModuleResolvers;

namespace ModularArchitecture.Services.Scaffolding
{
	/// <summary>
	/// Provides a simple example demonstrating how to set up a module service and its components.
	/// </summary>
	class Example
	{
		public void Initialize()
		{
			// Register the module resolver to be used:

			var service = Service.Instance;
			var resolver = new ManualModuleResolver<Modules>();

			resolver.Register<A>();
			resolver.Register<B>();

			service.Register(resolver);

			// Randomly create instances of modules. Note that the instantiation of IModuleC will fail,
			// as it has not been registered at the module resolver above!

			var a = service.Get<IModuleA>();
			var b = service.Get<IModuleB>();
			var c = service.Get<IModuleC>();

			a.DoSomething();
			b.DoSomethingElse();
			c.DoAnotherThing();

			// Randomly send a message over the message bus:

			service.MessageBus.Subscribe(a);
			service.MessageBus.Broadcast(Messages.Blubb, Modules.B);
		}
	}
}
