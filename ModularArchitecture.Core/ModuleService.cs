﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ModularArchitecture.Core.Communication;
using ModularArchitecture.Core.Exceptions;
using ModularArchitecture.Core.Extensions;

namespace ModularArchitecture.Core
{
	/// <summary>
	/// The main component managing all modules, thread-safe.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining module IDs.</typeparam>
	/// <typeparam name="TMessage">The type defining message values.</typeparam>
	public class ModuleService<TIdentifier, TMessage>
	{
		private readonly object busLock = new object();
		private readonly object serviceLock = new object();

		private IDictionary<Type, IModule<TIdentifier>> cache;
		private IList<ModuleResolver<TIdentifier>> resolvers;
		private MessageBus<TIdentifier, TMessage> bus;

		/// <summary>
		/// The message bus handling communication between all modules.
		/// </summary>
		public MessageBus<TIdentifier, TMessage> MessageBus
		{
			get
			{
				lock (busLock)
				{
					return bus ?? (bus = new MessageBus<TIdentifier, TMessage>());
				}
			}
		}

		public ModuleService()
		{
			cache = new Dictionary<Type, IModule<TIdentifier>>();
			resolvers = new List<ModuleResolver<TIdentifier>>();
		}

		/// <summary>
		/// Automatically creates an instance of all modules implementing <c>IAutoStartableModule</c>, utilizing the configured module
		/// resolvers. If a module happens to implement <c>IMessageRecipient</c>, the module will automatically be subscribed to the
		/// message bus. Singleton modules will not be re-instantiated if they already exist in the internal cache!
		/// <para />
		/// ATTENTION: The modules to be started must have an empty default constructor (== new() constraint)!
		/// </summary>
		/// <param name="args">Any optional parameters to be passed to the modules' start method.</param>
		public void AutoStart(params object[] args)
		{
			lock (serviceLock)
			{
				var start = new Action<IAutoStartableModule<TIdentifier>>((module) =>
				{
					if (module.StartAsync)
					{
						Task.Run(() => module.Start(args));
					}
					else
					{
						module.Start(args);
					}
				});

				foreach (var resolver in resolvers)
				{
					var modules = resolver.ResolveAll<IAutoStartableModule<TIdentifier>>(cache.Keys);

					foreach (var module in modules)
					{
						if (module.IsSingleton)
						{
							cache[module.GetType()] = module;
						}

						if (typeof(IMessageRecipient<TIdentifier, TMessage>).IsAssignableFrom(module.GetType()))
						{
							MessageBus.Subscribe(module as IMessageRecipient<TIdentifier, TMessage>);
						}

						start(module);
					}

					foreach (var cacheItem in cache.Where(kvp => typeof(IAutoStartableModule<TIdentifier>).IsAssignableFrom(kvp.Key)))
					{
						var singleton = cacheItem.Value as IAutoStartableModule<TIdentifier>;

						start(singleton);
					}
				}
			}
		}

		/// <summary>
		/// Starts an instance of the specified executable module. If the specified module type happens to implement
		/// <c>IMessageRecipient</c>, the executable module will automatically be subscribed to the message bus.
		/// <para />
		/// ATTENTION: The <c>IExecutableModule</c> to be executed must have an empty default constructor (== new() constraint)!
		/// </summary>
		/// <typeparam name="TModule">The type of the module to be started. Must be a class and inherit from <c>IExecutableModule</c>.</typeparam>
		/// <param name="args">Any optional parameters to be passed to the module.</param>
		public void Execute<TModule>(params object[] args) where TModule : class, IExecutableModule<TIdentifier>
		{
			var module = Get<TModule>();

			if (typeof(IMessageRecipient<TIdentifier, TMessage>).IsAssignableFrom(typeof(TModule)))
			{
				MessageBus.Subscribe(module as IMessageRecipient<TIdentifier, TMessage>);
			}

			module.Execute(args);
		}

		/// <summary>
		/// Asynchronously starts an instance of the specified executable module. If the specified module type happens to implement
		/// <c>IMessageRecipient</c>, the executable module will automatically be subscribed to the message bus.
		/// <para />
		/// ATTENTION: The <c>IExecutableModule</c> to be executed must have an empty default constructor (== new() constraint)!
		/// </summary>
		/// <typeparam name="TModule">The type of the module to be started. Must be a class and inherit from <c>IExecutableModule</c>.</typeparam>
		/// <param name="args">Any optional parameters to be passed to the module.</param>
		public void ExecuteAsync<TModule>(params object[] args) where TModule : class, IExecutableModule<TIdentifier>
		{
			Task.Run(() => Execute<TModule>(args));
		}

		/// <summary>
		/// Checks if the internal cache already contains a singleton module of the specified type.
		/// </summary>
		/// <typeparam name="TModule">The type of the module to be checked.</typeparam>
		/// <returns><c>true</c> if an instance already exists, otherwise <c>false</c>.</returns>
		public bool Exists<TModule>() where TModule : class, IModule<TIdentifier>
		{
			lock (serviceLock)
			{
				return cache.ContainsKey(typeof(TModule));
			}
		}

		/// <summary>
		/// Tries to create a new instance of the provided module type, optionally utilizing the given constructor arguments. If the
		/// specified module type is defined as singleton, the new instance will be stored in the internal cache upon first call and
		/// thenceforth retrieved from there.
		/// </summary>
		/// <typeparam name="TModule">The type of the module to be created. Must be a class and inherit from <c>IModule</c>.</typeparam>
		/// <param name="args">Any optional constructor arguments.</param>
		/// <exception cref="ModularArchitecture.Core.Exceptions.ModuleNotFoundException">In case the module cannot be resolved.</exception>
		public TModule Get<TModule>(params object[] args) where TModule : class, IModule<TIdentifier>
		{
			lock (serviceLock)
			{
				TModule instance = default(TModule);

				if (Exists<TModule>())
				{
					return cache[typeof(TModule)] as TModule;
				}

				foreach (var resolver in resolvers)
				{
					if (resolver.TryResolve(out instance, args))
					{
						break;
					}
				}

				if (instance == default(TModule))
				{
					throw new ModuleNotFoundException(typeof(TModule));
				}

				if (instance.IsSingleton)
				{
					cache[typeof(TModule)] = instance;
				}

				return instance;
			}
		}

		/// <summary>
		/// Removes the singleton instance of the specified type from the internal cache.
		/// </summary>
		/// <typeparam name="TModule">The type of the module to be removed.</typeparam>
		public void Remove<TModule>() where TModule : class, IModule<TIdentifier>
		{
			lock (serviceLock)
			{
				cache.Remove(typeof(TModule));
			}
		}

		/// <summary>
		/// Registers a module resolver to be used to when instantiating modules.
		/// </summary>
		public void Register(ModuleResolver<TIdentifier> resolver)
		{
			if (resolver == null)
			{
				return;
			}

			lock (serviceLock)
			{
				if (!resolvers.Contains(resolver))
				{
					resolvers.Add(resolver);
				}
			}
		}
	}
}
