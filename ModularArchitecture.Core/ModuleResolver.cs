﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;

namespace ModularArchitecture.Core
{
	/// <summary>
	/// Base class for module resolvers. A module resolver discovers and instantiates modules.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining module IDs.</typeparam>
	public abstract class ModuleResolver<TIdentifier>
	{
		protected abstract IList<Type> TypeRepository { get; }

		/// <summary>
		/// Instantiates an instance of each module that is assignable to the specified type. Skips all excluded types.
		/// <para />
		/// ATTENTION: The modules to be instantiated must have an empty default constructor (== new() constraint)!
		/// </summary>
		/// <typeparam name="TModule">The type defining the modules to be instantiated</typeparam>
		/// <param name="exclusions">A optional list of types that should be ignored.</param>
		/// <returns>A list containing the newly created modules, or an empty list if there were no assignable modules.</returns>
		public virtual IList<TModule> ResolveAll<TModule>(IEnumerable<Type> exclusions = null) where TModule : class, IModule<TIdentifier>
		{
			var modules = new List<TModule>();

			exclusions = exclusions ?? Enumerable.Empty<Type>();

			foreach (var type in TypeRepository)
			{
				var module = default(TModule);
				var excluded = exclusions.Any(t => t.Equals(type));

				if (!type.IsInterface && !type.IsAbstract && !excluded && typeof(TModule).IsAssignableFrom(type))
				{
					module = (TModule) Activator.CreateInstance(type);
					modules.Add(module);
				}
			}

			return modules;
		}

		/// <summary>
		/// Tries to instantiate a module of the provided type, utilizing the given constructor arguments, if any.
		/// </summary>
		/// <typeparam name="TModule">The type defining the module to be resolved.</typeparam>
		/// <returns><c>true</c> if the resolution was successfull.</returns>
		public virtual bool TryResolve<TModule>(out TModule module, params object[] args) where TModule : class, IModule<TIdentifier>
		{
			module = default(TModule);

			foreach (var type in TypeRepository)
			{
				if (!type.IsInterface && !type.IsAbstract && typeof(TModule).IsAssignableFrom(type))
				{
					module = (TModule) Activator.CreateInstance(type, args);

					break;
				}
			}

			return module != default(TModule);
		}
	}
}
