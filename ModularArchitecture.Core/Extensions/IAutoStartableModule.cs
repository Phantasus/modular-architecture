﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

namespace ModularArchitecture.Core.Extensions
{
	/// <summary>
	/// Defines an automatically startable module.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining module IDs.</typeparam>
	public interface IAutoStartableModule<TIdentifier> : IModule<TIdentifier>
	{
		/// <summary>
		/// Determines whether the module should be started asynchronously, i.e. in a new thread.
		/// </summary>
		bool StartAsync { get; }

		/// <summary>
		/// Defines the starting point called by the <c>ModuleService</c>.
		/// </summary>
		void Start(params object[] args);
	}
}
