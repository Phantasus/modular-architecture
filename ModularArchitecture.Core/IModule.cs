﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

namespace ModularArchitecture.Core
{
	/// <summary>
	/// The main interface from which all modules must inherit.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining module IDs.</typeparam>
	public interface IModule<TIdentifier>
	{
		/// <summary>
		/// The identifier uniquely identifying this module among all others.
		/// </summary>
		TIdentifier Id { get; }

		/// <summary>
		/// The name of this module, suitable e.g. for logging or debugging purposes.
		/// </summary>
		string Name { get; }

		/// <summary>
		/// Determines whether there can only exist one instance of this module type.
		/// </summary>
		bool IsSingleton { get; }
	}
}
