﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

namespace ModularArchitecture.Core.Communication
{
	internal class Message<TIdentifier, TMessage> : IMessage<TIdentifier, TMessage>
	{
		/// <summary>
		/// The sender of this message.
		/// </summary>
		public TIdentifier From { get; set; }

		/// <summary>
		/// The receiver of this message.
		/// </summary>
		public TIdentifier To { get; set; }

		/// <summary>
		/// The message itself.
		/// </summary>
		public TMessage Value { get; set; }

		/// <summary>
		/// Any optional parameters.
		/// </summary>
		public object[] Args { get; set; }

		/// <summary>
		/// Determines whether this message will be dispatched asynchronously, i.e. in a new thread.
		/// </summary>
		public bool IsAsync { get; set; }

		/// <summary>
		/// Determines whether the message will be sent to all recipients available.
		/// </summary>
		public bool IsBroadcast { get; set; }
	}
}
