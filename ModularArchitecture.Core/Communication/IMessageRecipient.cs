﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

namespace ModularArchitecture.Core.Communication
{
	/// <summary>
	/// This interface must be implemented to be able to subscribe to a <c>MessageBus</c>.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining recipient IDs.</typeparam>
	/// <typeparam name="TMessage">The type defining message values.</typeparam>
	public interface IMessageRecipient<TIdentifier, TMessage>
	{
		/// <summary>
		/// The ID of this recipient.
		/// </summary>
		TIdentifier Id { get; }

		/// <summary>
		/// Delivers a message to this recipient.
		/// </summary>
		void Receive(IMessage<TIdentifier, TMessage> message);
	}
}
