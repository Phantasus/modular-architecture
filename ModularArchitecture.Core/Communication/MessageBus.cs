﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ModularArchitecture.Core.Communication
{
	/// <summary>
	/// The message bus handles communication between modules and other components, thread-safe.
	/// </summary>
	/// <typeparam name="TIdentifier">The type defining sender and recipient IDs.</typeparam>
	/// <typeparam name="TMessage">The type defining message values.</typeparam>
	public class MessageBus<TIdentifier, TMessage>
	{
		private readonly object @lock = new object();

		private IList<IMessageRecipient<TIdentifier, TMessage>> bus;
		private IDictionary<Guid, IMessage<TIdentifier, TMessage>> cache;

		public MessageBus()
		{
			bus = new List<IMessageRecipient<TIdentifier, TMessage>>();
			cache = new Dictionary<Guid, IMessage<TIdentifier, TMessage>>();
		}

		/// <summary>
		/// Transmits a message as broadcast according to the specified parameters.
		/// </summary>
		public void Broadcast(TMessage value, TIdentifier from, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				From = from,
				Value = value,
				Args = args,
				IsBroadcast = true
			};

			Send(message);
		}

		/// <summary>
		/// Transmits a message as asynchronous broadcast according to the specified parameters.
		/// </summary>
		public void BroadcastAsync(TMessage value, TIdentifier from, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				From = from,
				Value = value,
				Args = args,
				IsAsync = true,
				IsBroadcast = true
			};

			Send(message);
		}

		/// <summary>
		/// Transmits a message according to the specified parameters.
		/// </summary>
		public void Send(TMessage value, TIdentifier to, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				To = to,
				Value = value,
				Args = args,
			};

			Send(message);
		}

		/// <summary>
		/// Transmits a message according to the specified parameters.
		/// </summary>
		public void Send(TMessage value, TIdentifier to, TIdentifier from, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				From = from,
				To = to,
				Value = value,
				Args = args,
			};

			Send(message);
		}

		/// <summary>
		/// Transmits a message asynchronously according to the specified parameters.
		/// </summary>
		public void SendAsync(TMessage value, TIdentifier to, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				To = to,
				Value = value,
				Args = args,
				IsAsync = true
			};

			Send(message);
		}

		/// <summary>
		/// Transmits a message asynchronously according to the specified parameters.
		/// </summary>
		public void SendAsync(TMessage value, TIdentifier to, TIdentifier from, params object[] args)
		{
			var message = new Message<TIdentifier, TMessage>
			{
				From = from,
				To = to,
				Value = value,
				Args = args,
				IsAsync = true
			};

			Send(message);
		}

		/// <summary>
		/// Subscribes a message recipient to the message bus.
		/// </summary>
		public void Subscribe(IMessageRecipient<TIdentifier, TMessage> recipient)
		{
			if (recipient == null)
			{
				return;
			}

			lock (@lock)
			{
				if (!bus.Contains(recipient))
				{
					bus.Add(recipient);
				}
			}
		}

		/// <summary>
		/// Unsubscribes a recipient from the message bus.
		/// </summary>
		public void Unsubscribe(IMessageRecipient<TIdentifier, TMessage> recipient)
		{
			lock (@lock)
			{
				bus.Remove(recipient);
			}
		}

		private void Send(IMessage<TIdentifier, TMessage> message)
		{
			lock (@lock)
			{
				var areEqual = new Func<TIdentifier, TIdentifier, bool>((a, b) => EqualityComparer<TIdentifier>.Default.Equals(a, b));
				var recipients = message.IsBroadcast ? bus.Where(r => !areEqual(r.Id, message.From)) : bus.Where(r => areEqual(r.Id, message.To));

				foreach (var recipient in recipients)
				{
					if (message.IsAsync)
					{
						Task.Run(() => recipient.Receive(message));
					}
					else
					{
						recipient.Receive(message);
					}
				}
			}
		}
	}
}
