﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModularArchitecture.Core;
using ModularArchitecture.Core.Exceptions;
using ModularArchitecture.UnitTests.MockObjects.ModuleResolver;
using ModularArchitecture.UnitTests.MockObjects.ModuleService;

namespace ModularArchitecture.UnitTests
{
	[TestClass]
	public class ModuleServiceTests
	{
		/// <summary>
		/// NOTE: This test does not fulfill the F.I.R.S.T. rule, in particular the "fast" aspect.
		/// </summary>
		[TestMethod]
		public void AsyncAutoStartableTest()
		{
			var startThreadId = Thread.CurrentThread.ManagedThreadId;
			var executionThreadId = Thread.CurrentThread.ManagedThreadId;
			var callback = new Action(() => executionThreadId = Thread.CurrentThread.ManagedThreadId);
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(AsyncAutoStartableMock) } };

			service.Register(resolver);
			service.AutoStart(callback);

			Thread.Sleep(100);

			Assert.AreNotEqual(startThreadId, executionThreadId);
		}

		/// <summary>
		/// NOTE: This test does not fulfill the F.I.R.S.T. rule, in particular the "fast" aspect.
		/// </summary>
		[TestMethod]
		public void AsyncExecutionableTest()
		{
			var startThreadId = Thread.CurrentThread.ManagedThreadId;
			var executionThreadId = Thread.CurrentThread.ManagedThreadId;
			var callback = new Action(() => executionThreadId = Thread.CurrentThread.ManagedThreadId);
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(AsyncExecutionableMock) } };

			service.Register(resolver);
			service.ExecuteAsync<AsyncExecutionableMock>(callback);
			
			Thread.Sleep(100);

			Assert.AreNotEqual(startThreadId, executionThreadId);
		}

		[TestMethod]
		public void AutoStartableTest()
		{
			const int ID = 3454;
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(AutoStartableMock) } };

			service.Register(resolver);
			service.AutoStart(ID);

			// AutoStartableMock is a singleton, hence we can get a reference to it...
			var startable = service.Get<AutoStartableMock>();

			Assert.AreEqual(ID, startable.Id);
		}

		[TestMethod]
		public void AutoStartableSingletonTest()
		{
			const int ID = 3454;
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(AutoStartableMock) } };

			service.Register(resolver);

			var referenceA = service.Get<AutoStartableMock>();

			service.AutoStart(ID);

			var referenceB = service.Get<AutoStartableMock>();

			Assert.AreEqual(ID, referenceA.Id);
			Assert.AreSame(referenceA, referenceB);
		}

		[TestMethod]
		public void AutoStartableMessageTransmissionTest()
		{
			const int SENDER_ID = -1;
			const string MESSAGE = "Hi there";
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(AutoStartableMock) } };

			service.Register(resolver);
			service.AutoStart();
			service.MessageBus.Broadcast(MESSAGE, SENDER_ID);

			// AutoStartableMock is a singleton, hence we can get a reference to it...
			var startable = service.Get<AutoStartableMock>();

			Assert.AreEqual(MESSAGE, startable.Message);
		}

		[TestMethod]
		public void ExecutionableTest()
		{
			const int ID = 43534;
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(ExecutableModuleMock) } };

			service.Register(resolver);
			service.Execute<ExecutableModuleMock>(ID);

			// ExecutableModuleMock is a singleton, hence we can get a reference to it...
			var executable = service.Get<ExecutableModuleMock>();

			Assert.AreEqual(ID, executable.Id);
		}

		[TestMethod]
		public void ExecutionableMessageTransmissionTest()
		{
			const int SENDER_ID = -1;
			const string MESSAGE = "Hi there";
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(ExecutableModuleMock) } };

			service.Register(resolver);
			service.Execute<ExecutableModuleMock>();
			service.MessageBus.Broadcast(MESSAGE, SENDER_ID);

			// ExecutableModuleMock is a singleton, hence we can get a reference to it...
			var executable = service.Get<ExecutableModuleMock>();

			Assert.AreEqual(MESSAGE, executable.Message);
		}

		[TestMethod]
		public void InstantiationTest()
		{
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock) } };

			service.Register(resolver);

			var instanceA = service.Get<IModule<int>>();
			var instanceB = service.Get<IntModuleMock>();

			Assert.IsNotNull(instanceA);
			Assert.IsNotNull(instanceB);
			Assert.IsInstanceOfType(instanceA, typeof(IntModuleMock));
			Assert.IsInstanceOfType(instanceB, typeof(IntModuleMock));
			Assert.AreNotSame(instanceA, instanceB);
		}

		[TestMethod]
		[ExpectedException(typeof(ModuleNotFoundException))]
		public void InstantiationOfUnknownTypeTest()
		{
			var service = new ModuleService<int, string>();

			service.Get<IntModuleMock>();
		}

		[TestMethod]
		public void RegistrationFailsafetyTest()
		{
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock) } };

			service.Register(null);
			service.Register(resolver);
			service.Get<IntModuleMock>();
		}

		[TestMethod]
		public void SingletonTest()
		{
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(SingletonModuleMock) } };

			service.Register(resolver);

			var referenceA = service.Get<SingletonModuleMock>();
			var referenceB = service.Get<SingletonModuleMock>();

			Assert.IsNotNull(referenceA);
			Assert.IsNotNull(referenceB);
			Assert.IsInstanceOfType(referenceA, typeof(SingletonModuleMock));
			Assert.IsInstanceOfType(referenceB, typeof(SingletonModuleMock));
			Assert.AreSame(referenceA, referenceB);
		}
		
		[TestMethod]
		public void SingletonRemovalTest()
		{
			var service = new ModuleService<int, string>();
			var resolver = new ResolverMock { Repository = new List<Type> { typeof(SingletonModuleMock) } };

			service.Register(resolver);

			var instanceA = service.Get<SingletonModuleMock>();

			service.Remove<SingletonModuleMock>();

			var instanceB = service.Get<SingletonModuleMock>();

			Assert.AreNotSame(instanceA, instanceB);
		}

		[TestMethod]
		public void SingletonRemovalFailsafetyTest()
		{
			var service = new ModuleService<int, string>();

			service.Remove<SingletonModuleMock>();
			service.Remove<IModule<int>>();
			service.Remove<SingletonModuleMock>();
		}
	}
}
