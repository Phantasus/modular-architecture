﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using ModularArchitecture.Core.Communication;

namespace ModularArchitecture.UnitTests.MockObjects.MessageBus
{
	class RecipientMock : IMessageRecipient<int, string>
	{
		public IList<IMessage<int, string>> Messages = new List<IMessage<int, string>>();

		public Action DeliveryCallback { get; set; }
		public int Id { get; set; }

		public RecipientMock(int id)
		{
			Id = id;
		}

		public void Receive(IMessage<int, string> message)
		{
			Messages.Add(message);

			if (DeliveryCallback != null)
			{
				DeliveryCallback();
			}
		}
	}
}
