﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using ModularArchitecture.Core;

namespace ModularArchitecture.UnitTests.MockObjects.ModuleResolver
{
	class ConstructorModuleMock : IModule<int>
	{
		public ConstructorModuleMock(int id)
		{
			Id = id;
		}

		public int Id { get; set; }

		public bool IsSingleton { get { return false; } }

		public string Name { get { return "Constructor Module Mock"; } }
	}
}
