﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System.Linq;
using ModularArchitecture.Core.Communication;
using ModularArchitecture.Core.Extensions;

namespace ModularArchitecture.UnitTests.MockObjects.ModuleService
{
	class AutoStartableMock : IAutoStartableModule<int>, IMessageRecipient<int, string>
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Message { get; set; }

		public bool IsSingleton
		{
			get { return true; }
		}

		public bool StartAsync
		{
			get { return false; }
		}

		public void Receive(IMessage<int, string> message)
		{
			Message = message.Value;
		}

		public void Start(params object[] args)
		{
			if (args != null && args.Any())
			{
				Id = (int) args[0];
			}
		}
	}
}
