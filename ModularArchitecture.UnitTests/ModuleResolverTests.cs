﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModularArchitecture.Core;
using ModularArchitecture.UnitTests.MockObjects.ModuleResolver;

namespace ModularArchitecture.UnitTests
{
	[TestClass]
	public class ModuleResolverTests
	{
		[TestMethod]
		public void BatchResolutionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock), typeof(IntModuleSubMock) } };
			var result = mock.ResolveAll<IModule<int>>();

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 2);
			Assert.IsInstanceOfType(result[0], typeof(IntModuleMock));
			Assert.IsInstanceOfType(result[1], typeof(IntModuleSubMock));
		}

		[TestMethod]
		public void BatchResolutionEmptyResultTest()
		{
			var mock = new ResolverMock();
			var result = mock.ResolveAll<IModule<int>>();

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 0);
		}

		[TestMethod]
		public void BatchResolutionExclusionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock), typeof(IntModuleSubMock) } };
			var result = mock.ResolveAll<IModule<int>>(new[] { typeof(IntModuleMock), typeof(IntModuleSubMock) });

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 0);

			result = mock.ResolveAll<IModule<int>>(new[] { typeof(IntModuleMock) });

			Assert.IsNotNull(result);
			Assert.IsTrue(result.Count == 1);
			Assert.IsInstanceOfType(result[0], typeof(IntModuleSubMock));
		}

		[TestMethod]
		public void ConstructorTypeResolutionTest()
		{
			const int ID = 3465;
			var mock = new ResolverMock { Repository = new List<Type> { typeof(ConstructorModuleMock) } };
			ConstructorModuleMock module = null;

			var result = mock.TryResolve(out module, ID);

			Assert.IsTrue(result);
			Assert.IsNotNull(module);
			Assert.IsInstanceOfType(module, typeof(ConstructorModuleMock));
			Assert.AreEqual(ID, module.Id);
		}

		[TestMethod]
		public void InterfaceResolutionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock) } };
			IModule<int> module = null;

			var result = mock.TryResolve(out module);

			Assert.IsTrue(result);
			Assert.IsNotNull(module);
			Assert.IsInstanceOfType(module, typeof(IModule<int>));
		}

		[TestMethod]
		public void RegisteredTypeResolutionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(IntModuleMock) } };
			IntModuleMock module = null;

			var result = mock.TryResolve(out module);

			Assert.IsTrue(result);
			Assert.IsNotNull(module);
			Assert.IsInstanceOfType(module, typeof(IntModuleMock));
		}

		[TestMethod]
		public void SpecializedTypeResolutionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(IntModuleSubMock) } };
			IntModuleMock module = null;

			var result = mock.TryResolve(out module);

			Assert.IsTrue(result);
			Assert.IsNotNull(module);
			Assert.IsInstanceOfType(module, typeof(IntModuleMock));
		}

		[TestMethod]
		public void UnregisteredTypeResolutionTest()
		{
			var mock = new ResolverMock { Repository = new List<Type> { typeof(NotAModuleMock) } };
			IntModuleMock module = null;

			var result = mock.TryResolve(out module);

			Assert.IsFalse(result);
			Assert.IsNull(module);
			Assert.IsNotInstanceOfType(module, typeof(IntModuleMock));
			Assert.AreEqual(module, default(IntModuleMock));
		}
	}
}
