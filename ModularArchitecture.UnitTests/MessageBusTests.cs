﻿/*
 * Copyright (C) 2017 Phantasus Software Systems
 *
 * This file is part of ModularArchitecture.
 *
 * ModularArchitecture is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ModularArchitecture is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ModularArchitecture. If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Linq;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModularArchitecture.Core.Communication;
using ModularArchitecture.UnitTests.MockObjects.MessageBus;

namespace ModularArchitecture.UnitTests
{
	[TestClass]
	public class MessageBusTests
	{
		/// <summary>
		/// NOTE: This test does not fulfill the F.I.R.S.T. rule, in particular the "fast" aspect.
		/// </summary>
		[TestMethod]
		public void AsyncTransmissionTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var deliveryThreadId = Thread.CurrentThread.ManagedThreadId;
			var transmissionThreadId = Thread.CurrentThread.ManagedThreadId;
			var callback = new Action(() => deliveryThreadId = Thread.CurrentThread.ManagedThreadId);
			var recipient = new RecipientMock(ID) { DeliveryCallback = callback };

			bus.Subscribe(recipient);
			bus.SendAsync("Blubb", ID);

			Thread.Sleep(100);

			Assert.AreNotEqual(transmissionThreadId, deliveryThreadId);
		}

		[TestMethod]
		public void BroadcastTest()
		{
			const int ID = 1;
			const int SENDER_ID = -1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Broadcast("Blubb", SENDER_ID);

			Assert.IsTrue(recipient.Messages.Count == 1);
			Assert.AreEqual(recipient.Messages.First().From, SENDER_ID);
			Assert.AreEqual(recipient.Messages.First().Value, "Blubb");
		}

		[TestMethod]
		public void BroadcastSenderExclusionTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Broadcast("Blubb", ID);

			Assert.IsTrue(recipient.Messages.Count == 0);
		}

		[TestMethod]
		public void SubscribeOnlyOnceTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Subscribe(recipient);
			bus.Subscribe(recipient);
			bus.Send("Blubb", ID);

			Assert.IsTrue(recipient.Messages.Count == 1);
		}

		[TestMethod]
		public void SubscriptionFailsafetyTest()
		{
			var bus = new MessageBus<int, string>();

			bus.Subscribe(null);
			bus.Send("Blubb", 1);
			bus.Subscribe(new RecipientMock(1));
			bus.Send("Blubb", 1);
		}

		[TestMethod]
		public void TransmissionTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Send("Blubb", ID);

			Assert.AreEqual(recipient.Messages.First().Value, "Blubb");
		}

		[TestMethod]
		public void TransmissionFailsafetyTest()
		{
			var bus = new MessageBus<int, string>();

			bus.Send(null, default(int));
			bus.Send("Blubb", default(int));
		}

		[TestMethod]
		public void TransmissionWithWrongIdTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Send("Blubb", 1000);

			if (recipient.Messages.Any())
			{
				Assert.Fail("Must not receive any message!");
			}
		}

		[TestMethod]
		public void TransmissionWithMultipleRecipientsTest()
		{
			var bus = new MessageBus<int, string>();
			var recipientA = new RecipientMock(1);
			var recipientB = new RecipientMock(2);
			var recipientC = new RecipientMock(3);

			bus.Subscribe(recipientA);
			bus.Subscribe(recipientB);
			bus.Subscribe(recipientC);
			bus.Send("Blubb", 1);

			Assert.AreEqual(recipientA.Messages.First().Value, "Blubb");

			if (recipientB.Messages.Any() || recipientC.Messages.Any())
			{
				Assert.Fail("B and C must not receive any message!");
			}
		}

		[TestMethod]
		public void TransmissionWithMultipleRecipientsAndMultipleMessagesTest()
		{
			var bus = new MessageBus<int, string>();
			var recipientA = new RecipientMock(1);
			var recipientB = new RecipientMock(2);
			var recipientC = new RecipientMock(3);

			bus.Subscribe(recipientA);
			bus.Subscribe(recipientB);
			bus.Subscribe(recipientC);
			bus.Send("Blubb", 1);
			bus.Send("Blabb", 3);

			Assert.AreEqual(recipientA.Messages.First().Value, "Blubb");
			Assert.AreEqual(recipientC.Messages.First().Value, "Blabb");

			if (recipientB.Messages.Any())
			{
				Assert.Fail("B must not receive any message!");
			}
		}

		[TestMethod]
		public void UnsubscriptionTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Subscribe(recipient);
			bus.Unsubscribe(recipient);
			bus.Send("Blabb", ID);

			if (recipient.Messages.Any())
			{
				Assert.Fail("Must not receive any message!");
			}
		}

		[TestMethod]
		public void UnsubscriptionFailsafetyTest()
		{
			const int ID = 1;
			var bus = new MessageBus<int, string>();
			var recipient = new RecipientMock(ID);

			bus.Unsubscribe(null);
			bus.Unsubscribe(recipient);
			bus.Unsubscribe(recipient);
			bus.Unsubscribe(recipient);
		}
	}
}
